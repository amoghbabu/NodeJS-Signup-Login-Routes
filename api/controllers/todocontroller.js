const Task = require('../models/todomodel');
const ObjectId = require('mongoose').Types.ObjectId;
//to get task of particular id
exports.getTask = (req, res) => {
    var id = new ObjectId(req.params.taskId);
    Task.findById(id, function(err, Task) {
    if (err)
        res.send(err);
    res.json(Task);
    });
}
//to get all the tasks
exports.getTasks = (req, res)=>{   
    Task.find().select({"__v":0}).then(data=>{
            res.json(data);
        }).catch(err=>{
            res.send(err);
        })
}
// create a task
exports.createTask = (req, res) => {
    let newTask = new Task({
        name: req.body.name,
        description: req.body.description,
        status: "ongoing"
    });
    newTask.save().then(data => {
        res.json(data);
    }).catch(err=>{
        res.send(err);
    })
}
// read a single task 
exports.readTask = (req, res) => {
    Task.findById(req.params.id, (err, task) => {
        if (err)
            res.send(err);
        newFunction(res, task);
    });
};
// update a particular task 
exports.updateTask = (req, res) => {
    var id = new ObjectId(req.params.taskId);
    const data = {
        name:req.body.name,
        description:req.body.description
    }
    Task.findOneAndUpdate({ _id: id }, data, { new: true }, (err, task) => {
        if (err)
            res.send(err);
        res.json(task);
    });
};
// delete a single task 
exports.deleteTask = (req, res) => {
    var id = new ObjectId(req.params.taskId);
    Task.remove({
        _id: id
    }, (err, task) => {
        if (err)
            res.send(err);
        res.json({ message: 'Task deleted!!' });
    });
};
function newFunction(res, task) {
    res.json(task);
}